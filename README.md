# koleksi
Koleksi Repository Yang di Pantau / Diikuti / Menarik - Istilah lain dari awesome versi pribadi.

Daripada disimpan di lokal, sekalian saja di sini. Mungkin saja ada yang ikutan memanfaatkan.

[![Visits Badge](https://badges.pufler.dev/visits/banghasan/koleksi)](https://badges.pufler.dev) ![updates](https://badges.pufler.dev/updated/banghasan/koleksi)

## Server

### Tunneling

- https://github.com/anderspitman/awesome-tunneling
- https://localhost.run/
- https://github.com/fatedier/frp/

#### Other

- https://github.com/yudai/gotty

### Web

- https://github.com/caddyserver/caddy
- https://github.com/gohugoio/hugo
- https://github.com/go-gitea/gitea 

### OpenWrt

- https://github.com/ophub/amlogic-s9xxx-openwrt - untuk STB

### Armbian

- https://github.com/ophub/amlogic-s9xxx-armbian


## System

- https://github.com/flameshot-org/flameshot
- https://github.com/ngoduykhanh/wireguard-ui
- https://github.com/firezone/firezone
- https://github.com/asciimoo/wuzz - HTTP Request

### Proxy

- https://github.com/txthinking/brook

### font

- https://github.com/xz/fonts

## Android

- https://github.com/Genymobile/scrcpy - tampilkan ke layar pc / laptop


## Ads

- https://github.com/StevenBlack
- https://github.com/AdguardTeam/AdGuardHome
- https://github.com/ABPindo/indonesianadblockrules/

## Website

### Domain

- https://sslip.io/
-  list blacklist dari `trustpositif.kominfo.go.id` -> buat repo sendiri saja nanti

## Coding

### Bash

- https://github.com/google/zx

### NodeJS

- https://github.com/nodesource/distributions/blob/master/README.md - Install di Ubuntu
